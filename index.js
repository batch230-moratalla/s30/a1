// activity 30

// 1. Result of using MongoDB Aggregation to count the total number of fruits on sale.
// Insert your query below... 


// 2. Result of using MongoDB Aggregation to count the total number of fruits with stock more than 20.
// Insert your query below... 


// 3. Result of using MongoDB Aggregation to get the average price of fruits onSale per supplier
// Insert your query below... 


// 4. Result of using MongoDB Aggregation to get the highest price of a fruit per supplier
// Insert your query below... 


// 5. Result of using MongoDB Aggregation to to get the lowest price of a fruit per supplier.
// Insert your query below... 


// Activity Guide:
/*
1. Create an activity.js file on where to write and save the solution for the activity.
2. Use the count operator to count the total number of fruits on sale.
3. Use the count operator to count the total number of fruits with stock more than 20.
4. Use the average operator to get the average price of fruits onSale per supplier.
5. Use the max operator to get the highest price of a fruit per supplier.
6. Use the min operator to get the lowest price of a fruit per supplier.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
9. Add the link in Boodle.
*/


db.fruits.aggregate(
		[
			{$match: {onSale: true}},
			{$count: "fruitsOnSale"}
		]
	);

db.fruits.aggregate(
		[
			{$match: {stock: {$gte: 20}}},
			{$count: "enoughStock"}
		]
	);

db.fruits.aggregate(
	[
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
	]
);


db.fruits.aggregate(
	[
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
	]
);

db.fruits.aggregate(
	[
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", max_price: {$min: "$price"}}},
		{$sort: {total: -1}}
	]
);